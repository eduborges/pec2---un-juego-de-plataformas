﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controla los powerUp */

public class MushroomController : MonoBehaviour {

    /// <summary>
    /// Velocidad de desplazamiento
    /// </summary>
    [SerializeField]
    float speed = 3f;

    /// <summary>
    /// Variable auxiliar para manejar los cambios de velocidad
    /// </summary>
    float internalSpeed;

    /// <summary>
    /// Puntos que otorga al recogerse
    /// </summary>
    [SerializeField]
    int pointsValue = 1000;

    /// <summary>
    /// Rigidbody del powerUp
    /// </summary>
    Rigidbody2D MushRb;

    /// <summary>
    /// Controlador del jugador
    /// </summary>
    PlayerController playerController;

    /// <summary>
    /// Controlador de la interfaz
    /// </summary>
    UIManager uiManager;

    /// <summary>
    /// Semáforo que indica cuando ha sido obtenido el powerUp
    /// </summary>
    bool mutexAlreadyTaken;

    // Inicialización
    void Start () {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
        AudioAgent.powerUpAppears.GetComponent<AudioSource>().Play();
        MushRb = GetComponent<Rigidbody2D>();
        MushRb.isKinematic = true;                      // Hasta que no termine de spawnear no tendrá un rigidbody dinámico
        mutexAlreadyTaken = false;
        internalSpeed = 0;
    }
	
	// Ajusta la velocidad a cada frame
	void Update () {
        MushRb.velocity = new Vector2(internalSpeed, 0f);
    }

    /// <summary>
    /// Animación de aparición del powerUp
    /// </summary>
    /// <returns>IEnumerator</returns>
    public IEnumerator popUp() {
        float posY = 0.025f;

        for (int i = 0; i < 40; i++) {
            transform.position = new Vector3(transform.position.x, transform.position.y + posY, transform.position.z);
            yield return new WaitForSeconds(0.025f);
        }
        MushRb.isKinematic = false;
        internalSpeed = speed;
    }

    /// <summary>
    /// Cambia el sentido de la velocidad y el sprite del powerUp
    /// </summary>
    public void Flip() {
        GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        internalSpeed = -internalSpeed;
    }

    /// <summary>
    /// Acciones a realizar cuando detecta un trigger
    /// </summary>
    /// <param name="col">Detalles del collider</param>
    void OnTriggerEnter2D(Collider2D col) {
        if (!MushRb.isKinematic) {          
            if (col.transform.tag != "Player" && col.transform.tag != "floor") {
                Flip();                                                             
            }

            if (col.transform.tag == "Player" && !mutexAlreadyTaken) {
                mushroomTaken();
            }
            
            if(col.transform.tag == "hellfire") {                   // Si cae al vacío
                Destroy(gameObject);
            }
        }
    }

    /// <summary>
    /// Aplica al jugador el powerUp y añade los puntos que vale
    /// </summary>
    private void mushroomTaken() {
        AudioAgent.powerUpTaken.GetComponent<AudioSource>().Play();
        mutexAlreadyTaken = true;
        uiManager.AddPoints(pointsValue);
        playerController.pickUpMushroom();
        Destroy(gameObject);
    }
}
