﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que define las características del bloque tipo interrogación */

public class InterrogationBlock : Block {

    /// <summary>
    /// Constructor por defecto
    /// </summary>
    /// <param name="amount">Cantidad de contenido</param>
    /// <param name="content">Tipo de contenido</param>
    public InterrogationBlock(int amount, GameObject content) {
        aspect = Resources.Load<Sprite>("InterrogationBlock");
        prefabContent = content;
        currentAmount = amount;
        colorChanging = true;
    }

}
