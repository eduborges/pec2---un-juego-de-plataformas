﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase base que representa los elementos básicos de un bloque */
public class Block {

    /// <summary>
    /// Aspecto que tendrá el bloque
    /// </summary>
    public Sprite aspect;

    /// <summary>
    /// ¿Se puede destruir el bloque?
    /// </summary>
    public bool destructible = false;

    /// <summary>
    /// Si un bloque recibe un cabezazo y hay un gooma encima ¿tiene la capacidad el bloque de matar al gooma?
    /// </summary>
    public bool canKillGooma = false;

    /// <summary>
    /// ¿El bloque es rígido o al golpearse hace animación de moverse?
    /// </summary>
    public bool movable = true;

    /// <summary>
    /// ¿El color del bloque cambia con el tiempo?
    /// </summary>
    public bool colorChanging = false;

    /// <summary>
    /// Cantidad de contenido que contiene el bloque
    /// </summary>
    public int currentAmount;

    /// <summary>
    /// Tipo de contenido que tiene el bloque
    /// </summary>
    public GameObject prefabContent;

    /// <summary>
    /// Comprueba si el bloque está vacío o tiene más contenido
    /// </summary>
    /// <returns>¿El bloque está vacío?</returns>
    public bool empty() {
        if (currentAmount > 0)
            return false;
        else
            return true;
    }

    /// <summary>
    /// Comprueba si existe contenido dentro del bloque
    /// </summary>
    /// <returns>¿Tiene contenido asignado?</returns>
    public bool hasContent() {
        if (prefabContent != null)
            return true;
        else
            return false;
    }
}
