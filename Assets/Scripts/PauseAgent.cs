﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona el panel de pausa */

public class PauseAgent : MonoBehaviour {

    /// <summary>
    /// Conocer el estado de pausa del juego
    /// </summary>
    bool paused = false;

    /// <summary>
    /// Panel que se muestra cuando el juego está pausado
    /// </summary>
    public GameObject panelPause;
	
	// Comprobar a cada frame si se ha pulsado Esc para pausar
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (paused)
                unpause();
            else
                pause();
        }
	}

    /// <summary>
    /// Termina la pausa del tiempo
    /// </summary>
    public void unpause() {
        Time.timeScale = 1f;
        panelPause.SetActive(false);
        paused = false;
    }

    /// <summary>
    /// Pausa el tiempo
    /// </summary>
    void pause() {
        Time.timeScale = 0f;
        panelPause.SetActive(true);
        paused = true;
    }
}
