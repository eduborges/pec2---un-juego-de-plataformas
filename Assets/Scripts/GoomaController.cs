﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que controlla el comporamiento de los enemigos */

public class GoomaController : MonoBehaviour {

    /// <summary>
    /// Aspecto que tendrá el enemigo al ser aplastado por el personaje
    /// </summary>
    public Sprite smashedSprite;

    /// <summary>
    /// Velocidad en el eje x de los enemigos
    /// </summary>
    [SerializeField]
    float speed = -4f;

    /// <summary>
    /// Puntos que da matar un enemigo
    /// </summary>
    [SerializeField]
    int pointsValue = 100;

    /// <summary>
    /// Controlador del jugador
    /// </summary>
    PlayerController playerKeko;

    /// <summary>
    /// Estado del enemigo usado como semáforo para no obtener comportamientos inesperados entre colisiones
    /// </summary>
    bool dead = false;

    /// <summary>
    /// Rigidbody del enemigo
    /// </summary>
    Rigidbody2D goomaRb;

    /// <summary>
    /// Estado del enemigo que le desactiva el movimiento, usado para que los enemigos aparezcan en los mismos lugares independentemiente de lo que tarde el jugador en avanzar
    /// </summary>
    bool freezed = true;

	// Use this for initialization
	void Start () {
        playerKeko = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        goomaRb = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void Update () {
        if(!freezed)
            goomaRb.velocity = new Vector2(speed, goomaRb.velocity.y);

	}

    /// <summary>
    /// Acciones a realizar cuando la cabeza del personaje detecta un trigger
    /// </summary>
    /// <param name="col">Detalles del collider</param>
    void OnTriggerEnter2D(Collider2D col) {
        
        if (col.transform.tag == "Player" && !dead) {       // Si la cabeza detecta que es el jugador
            StartCoroutine(killGooma(true));
            playerKeko.miniJump();
        }

        if(col.transform.tag == "MainCamera") {             // Si detecta el paso de la cámara
            freezed = false;
        }
    }

    /// <summary>
    /// Acciones a realizar cuando el enemigo detecta una colisión
    /// </summary>
    /// <param name="col">Detalles de la colisiones</param>
    void OnCollisionEnter2D(Collision2D col) {

        if (col.transform.tag == "Player"){
            if (!dead && !playerKeko.immune) {
                playerKeko.getDamaged();                // Daña al jugador si no es inmune y no está muerto el enemigo
            }
        } else if (col.transform.tag == "hellfire") {
            Destroy(gameObject);                        // Cuando caen al vacío se destruyen para ahorrar recursos
        } else if(col.transform.tag != "floor") {
            Flip();                                     // Si choca contra algo que no sea suelo o algo de lo anterior cambia el sentido
        }

    }

    /// <summary>
    /// Cambiar el sentido del vector de velocidad
    /// </summary>
    void Flip() {
        GetComponent<SpriteRenderer>().flipX = !GetComponent<SpriteRenderer>().flipX;
        speed = -speed;
    }


    /// <summary>
    /// Mata al enemigo y realiza las acciones necesarias dependiendo de cada tipo de muerte
    /// </summary>
    /// <param name="smashed">¿Muerte por aplastamiento?</param>
    /// <returns></returns>
    public IEnumerator killGooma(bool smashed) {

        dead = true;
        GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>().AddPoints(pointsValue);
        AudioAgent.goomaKill.GetComponent<AudioSource>().Play();
        if (smashed) {                                                  // Si muere por aplastamiento del jugador
            goomaRb.velocity = new Vector2(0f, 0f);
            GetComponent<SpriteRenderer>().sprite = smashedSprite;
            yield return new WaitForSeconds(0.3f);
            if (gameObject != null)
                Destroy(gameObject);
        } else {                                                        // Si muere por bloque
            miniJump();
            GetComponent<SpriteRenderer>().flipY = true;
            foreach(CircleCollider2D cc in GetComponents<CircleCollider2D>()){
                cc.enabled = false;
            }
            yield return new WaitForSeconds(3f);
            if(gameObject != null)
                Destroy(gameObject);
        }
    }

    /// <summary>
    /// Aplica una fueza de salto en el eje vertical
    /// </summary>
    void miniJump() {
        goomaRb.velocity = new Vector2(goomaRb.velocity.x, 10f);
    }
}
