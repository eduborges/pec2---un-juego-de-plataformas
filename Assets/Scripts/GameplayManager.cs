﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

/* Clase que gestiona las rondas y la sincronización del a partida */

public class GameplayManager : MonoBehaviour {

    /// <summary>
    /// Panel que muestra las rondas actuales
    /// </summary>
    public GameObject roundPanel;

    /// <summary>
    /// Panel que muestra que la partida ha terminado
    /// </summary>
    public GameObject gameOverPanel;

    /// <summary>
    /// Items que hay que desactivar mientras se muestra algún panel
    /// </summary>
    [SerializeField]
    GameObject[] toDisable;

    /// <summary>
    /// Display de texto que muestra el número de rondas que quedan
    /// </summary>
    [SerializeField]
    GameObject roundDisplay;

    /// <summary>
    /// Número de rondas actuales que quedan
    /// </summary>
    static int roundLeft = 3;

    /// <summary>
    /// Controlador de la interfaz con el que comunicarse
    /// </summary>
    UIManager uiManager;

    // Inicialización
    void Start() {
        uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
        pauseTime();
    }

    /// <summary>
    /// Termina la ronda y comprueba si quedan o se ha terminado la partida
    /// </summary>
    public void roundOver() {
        roundLeft--;
        if(roundLeft < 0) {
            gameOver();
        } else {
            restartLevel();
        } 
    }

    /// <summary>
    /// Activa el panel que muestra la pantalla de fin de partida
    /// </summary>
    void gameOver() {
        gameOverPanel.SetActive(true);
        resetRounds();
    }

    /// <summary>
    /// Resetea el número de rondas al original
    /// </summary>
    public static void resetRounds() {
        roundLeft = 3;
    }

    /// <summary>
    /// Desactiva los items definidos mientras se muestra el pandel de rondas
    /// </summary>
    void pauseTime() {
        roundDisplay.GetComponent<Text>().text = roundLeft.ToString();
        roundPanel.SetActive(true);
        enableGameItems(false);
        Invoke("unpauseTime", 3);
    }

    /// <summary>
    ///  Se desactiva el panel de rondas y reactiva los items que habían sido desactivados
    /// </summary>
    void unpauseTime() {
        roundPanel.SetActive(false);
        enableGameItems(true);
        uiManager.startTiming();
    }

    public void restartLevel() {
        SceneAgent.reloadScene();
    }

    void enableGameItems(bool active) {

        playerMovement(active);

        foreach (GameObject item in toDisable) {
            item.SetActive(active);
        }
    }

    void playerMovement(bool active) {
        GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>().enabled = active;
    }

    void levelWon() {
        SceneAgent.victoryScreen();
    }

    void saveScores() {
        VictoryAgent.chocopoints = uiManager.currentScore;
        VictoryAgent.coins = uiManager.currentCoins;
    }

    void OnTriggerEnter2D(Collider2D col) {
        if (col.transform.tag == "Player"){
            Destroy(col.transform.gameObject);
            saveScores();
            Invoke("levelWon", 3f);
        }   
    }
}
