﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que define las características del bloque tipo brick */

public class BrickBlock : Block {

    /// <summary>
    /// Constructor por defecto
    /// </summary>
    public BrickBlock() {
        destructible = true;
        canKillGooma = true;
        aspect = Resources.Load<Sprite>("InfernalBrick");
    }


    /// <summary>
    /// Constructor de un bloque brick con contenido
    /// </summary>
    /// <param name="amount">Cantidad de contenido</param>
    /// <param name="content">Tipo de contenido</param>
    public BrickBlock(int amount, GameObject content) {
        destructible = false;
        canKillGooma = true;
        aspect = Resources.Load<Sprite>("InfernalBrick");
        prefabContent = content;
        currentAmount = amount;
    }
}
