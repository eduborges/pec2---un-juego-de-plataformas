﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que centraliza todos los audios usados en el juego */
public class AudioAgent : MonoBehaviour {
 
    /// <summary>
    /// Efecto sonoro de salto
    /// </summary>
    [SerializeField]
    GameObject _jump;
    public static GameObject jump;

    /// <summary>
    /// Efecto sonoro de obtención de una moneda
    /// </summary>
    [SerializeField]
    GameObject _coinPick;
    public static GameObject coinPick;

    /// <summary>
    /// Efecto sonoro de destrución de un bloque
    /// </summary>
    [SerializeField]
    GameObject _blockDestroy;
    public static GameObject blockDestroy;

    /// <summary>
    /// Efecto sonoro de la eliminación de un enemigo
    /// </summary>
    [SerializeField]
    GameObject _goomaKill;
    public static GameObject goomaKill;

    /// <summary>
    /// Efecto sonoro de aparición de un powerUp
    /// </summary>
    [SerializeField]
    GameObject _powerUpAppears;
    public static GameObject powerUpAppears;

    /// <summary>
    /// Efecto sonoro de obtención de un powerUp
    /// </summary>
    [SerializeField]
    GameObject _powerUpTaken;
    public static GameObject powerUpTaken;

    /// <summary>
    /// Efecto sonoro de recibir daño
    /// </summary>
    [SerializeField]
    GameObject _damageTaken;
    public static GameObject damageTaken;

    /// <summary>
    /// Efecto sonoro de completar el nivel
    /// </summary>
    [SerializeField]
    GameObject _victory;
    public static GameObject victory;

    // Inicialización de los audios
    void Start() {
        jump = _jump;
        coinPick = _coinPick;
        blockDestroy = _blockDestroy;
        goomaKill = _goomaKill;
        powerUpAppears = _powerUpAppears;
        powerUpTaken = _powerUpTaken;
        damageTaken = _damageTaken;
        victory = _victory;
    }

}
