﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que se encarga de ajustar un texto UI a un GameObject del mundo */

public class WorldTextController : MonoBehaviour {

    /// <summary>
    /// GameObject al que está ligado
    /// </summary>
    public GameObject attachedTo;

	// Inicialización
	void Start () {
        transform.position = Camera.main.WorldToScreenPoint(new Vector3(attachedTo.transform.position.x, attachedTo.transform.position.y + attachedTo.GetComponent<SpriteRenderer>().bounds.size.y));
    }
	
	void Update () {
        transform.position = Camera.main.WorldToScreenPoint(new Vector3(attachedTo.transform.position.x, attachedTo.transform.position.y + attachedTo.GetComponent<SpriteRenderer>().bounds.size.y));
    }
}
