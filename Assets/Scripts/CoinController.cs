﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Controlador de las monedas contenidas en los bloques */

public class CoinController : MonoBehaviour {

    /// <summary>
    /// Valor en puntos que tiene la moneda al obtenerla
    /// </summary>
    [SerializeField]
    int pointsValue = 100;

    // Inicialización
    void Start() {
        AudioAgent.coinPick.GetComponent<AudioSource>().Play(); // Cuando aparece la moneda, hace su sonido
    }

    /// <summary>
    /// Animación de la moneda (subir y bajar)
    /// </summary>
    /// <returns>IEnumerator</returns>
    public IEnumerator popUp() {
        float posY = 0.2f;

        // Se actualizan los puntos y monedas en la interfaz
        UIManager uiManager = GameObject.FindGameObjectWithTag("uimanagement").GetComponent<UIManager>();
        uiManager.AddPoints(pointsValue);
        uiManager.AddCoin();

        for (int i = 0; i < 37; i++) {
            if (i == 20)
                posY = -posY;
            transform.position = new Vector3(transform.position.x, transform.position.y + posY, transform.position.z);
            yield return new WaitForSeconds(0.01f);
        }
        Destroy(gameObject);
    }
}
