﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/* Clase que gestiona la posición del collider que encierra al personaje en los bordes de la cámara */

public class CameraHelper : MonoBehaviour {

    /// <summary>
    /// Distancia del mundo entre el centro de la pantalla y los bordes
    /// </summary>
    Vector2 screenDistance;

    /// <summary>
    /// Posición actual de la cámara principal
    /// </summary>
    Vector3 cameraPos;

    /// <summary>
    /// Collider que bloqueará el retroceso del personaje en el mapa
    /// </summary>
    BoxCollider2D leftCollider;

	// Inicialización
	void Start () {
        calculateCameraData();
        leftCollider = gameObject.AddComponent<BoxCollider2D>();
        attachCollider();
    }
	
	// Actualizar distancias y posiciones en cada frame
	void Update () {
        calculateCameraData();
        attachCollider();
    }

    /// <summary>
    /// Calcula la posición de la cámara y las distancias actuales
    /// </summary>
    void calculateCameraData() {
        cameraPos = Camera.main.transform.position;
        screenDistance.x = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, 0))) * 0.5f;
        screenDistance.y = Vector2.Distance(Camera.main.ScreenToWorldPoint(new Vector2(0, 0)), Camera.main.ScreenToWorldPoint(new Vector2(0, Screen.height))) * 0.5f;
    }

    /// <summary>
    /// Asigna la nueva posición al collider
    /// </summary>
    void attachCollider() {
        // La posición del collider izquierdo será la posición de la cámara - la mitad del ancho de la pantalla - 
        // la mitad del ancho del collider
        transform.position = new Vector3(cameraPos.x - screenDistance.x - (leftCollider.size.x * 0.5f), cameraPos.y);

        // Su altura será la altura de la pantalla
        leftCollider.size = new Vector2(leftCollider.size.x, screenDistance.y * 2f);
    }
}
