﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    /// <summary>
    /// Velocidad básica del personaje
    /// </summary>
    [SerializeField]
    float _speed;

    /// <summary>
    /// Velocidad adicional añadida cuando se esprinta
    /// </summary>
    [SerializeField]
    float _extraSpeedOnSprint;

    /// <summary>
    /// Altura del salto más pequeño posible de hacer
    /// </summary>
    [SerializeField]
    float jumpHeight;

    /// <summary>
    /// Tiempo de salto máximo
    /// </summary>
    [SerializeField]
    float jumpTime;

    /// <summary>
    /// Cantidad de escalado del personaje al obtener un powerUp
    /// </summary>
    [SerializeField]
    float powerUpScale;

    /// <summary>
    /// Collider que representa la cabeza del personaje
    /// </summary>
    public CircleCollider2D headCollider;

    /// <summary>
    /// Collider que representa los pies del personaje
    /// </summary>
    public CircleCollider2D groundCollider;

    /// <summary>
    /// Máscara que define que objetos son suelo
    /// </summary>
    public LayerMask whatIsGround;

    /// <summary>
    /// Posibles estados de poder del personaje
    /// </summary>
    public enum PlayerPower { Basic, BigBasic}

    /// <summary>
    /// Estado de poder actual del personaje
    /// </summary>
    PlayerPower _currentPower;

    /// <summary>
    /// Variable auxiliar que evita realizar multiples saltos al mantener pulsado el botón de salto
    /// </summary>
    bool stopJump = false;

    /// <summary>
    /// Contador que mide el tiempo que lleva el jugador saltando
    /// </summary>
    float jumpTimeCounter;

    /// <summary>
    /// Vector de movimiento que calcula las velocidad que se desean alcanzar
    /// </summary>
    Vector2 movement;

    /// <summary>
    /// Animator del jugador
    /// </summary>
    Animator playerAnimator;

    /// <summary>
    /// Rigidbody del jugador
    /// </summary>
    Rigidbody2D playerRigidbody2D;

    /// <summary>
    /// Velocidad actual modificado por SmoothDamp para generar un efecto de aceleración
    /// </summary>
    Vector2 velocity = Vector2.zero;

    /// <summary>
    /// Identifica si el jugador tiene los pies en el suelo
    /// </summary>
    bool _grounded;

    /// <summary>
    /// Identifica si el jugador es inmune al daño de lo enemigos
    /// </summary>
    bool _immune;

    // Propiedades

    public float speed {
        get { return _speed; }
        set { _speed = value; }
    }

    public float extraSpeedOnSprint {
        get { return _extraSpeedOnSprint; }
        set { _extraSpeedOnSprint = value; }
    }

    public bool grounded {
        get { return _grounded; }
        set { _grounded = value; }
    }

    public PlayerPower currentPower {
        get { return _currentPower; }
        set { _currentPower = value; }
    }

    public bool immune {
        get { return _immune; }
        set { _immune = value; }
    }

    // Preinicialización
    void Awake() {
        playerAnimator = GetComponent<Animator>();
        playerRigidbody2D = GetComponent<Rigidbody2D>();   
    }

    // Inicialización
    void Start() {
        immune = false;
        currentPower = PlayerPower.Basic;
    }

    // Comprueba a cada frame si se está pisando el suelo
    void Update() {
        checkGround();
    }


    /// <summary>
    /// Mueve al personaje en la dirección del eje horizontal
    /// </summary>
    /// <param name="h">valor eje horizontal</param>
    public void Move(float horizontalAxisValue) {

        movement.Set(horizontalAxisValue, 0);
        Flip(horizontalAxisValue);
        movement = movement * speed;
        playerAnimator.SetFloat("Anim_movement", Mathf.Abs(horizontalAxisValue));

        Vector2 targetVelocity = new Vector2(movement.x * 10f * Time.fixedDeltaTime, playerRigidbody2D.velocity.y);
        if (!grounded)                                                                                                             // Si está en el aire, tarda más en obtener la velocidad deseada
            playerRigidbody2D.velocity = Vector2.SmoothDamp(playerRigidbody2D.velocity, targetVelocity, ref velocity, 0.5f);
        else {
            playerRigidbody2D.velocity = targetVelocity;
        }
    }

    /// <summary>
    /// Provoca que el personaje salte dependiendo del tiempo que se mantenga pulsado hasta un límite definido en jumpTime
    /// </summary>
    /// <param name="jumpValue">Estado del salto</param>
    public void Jump(PlayerMovement.JumpState jumpValue) {
        if (jumpValue == PlayerMovement.JumpState.StartJump) {          // Inicio del salto
            PlayerMovement.KeyDownSync = false;
            if (grounded) {
                miniJump();
                stopJump = false;
                AudioAgent.jump.GetComponent<AudioSource>().Play();
            }
        }

        if (jumpValue == PlayerMovement.JumpState.Jumping && !stopJump) {   // Continuación del salto
            if (jumpTimeCounter > 0) {
                miniJump();
                jumpTimeCounter -= Time.fixedDeltaTime ;
            }
        }

        if (jumpValue == PlayerMovement.JumpState.StopJumping) {            // Caída del sato
            PlayerMovement.KeyUpSync = false;
            jumpTimeCounter = 0;
            stopJump = true;
        }
    }


    /// <summary>
    /// Gira al personaje hacia el lado al que esté avanzando
    /// </summary>
    /// <param name="h">valor del eje horizontal</param>
    void Flip(float h) {
        if (h < 0)
            GetComponent<SpriteRenderer>().flipX = false;
        else if (h > 0)
            GetComponent<SpriteRenderer>().flipX = true;
    }

    /// <summary>
    /// Comprueba que los pies están tocando el suelo
    /// </summary>
    void checkGround() {
        grounded = Physics2D.OverlapCircle(transform.localPosition + (Vector3)groundCollider.offset * groundCollider.transform.localScale.x, groundCollider.radius * transform.localScale.y, whatIsGround);

        if (grounded) {
            stopJump = true;
            jumpTimeCounter = jumpTime;
            playerAnimator.SetBool("Anim_jump", false);
        }
        else {
            playerAnimator.SetBool("Anim_jump", true);
        }
    }

    /// <summary>
    /// Provoca que el jugador reciba 1 golpe de daño
    /// </summary>
    public void getDamaged() {
        AudioAgent.damageTaken.GetComponent<AudioSource>().Play();
        switch (currentPower) {
            case PlayerPower.BigBasic: {
                StartCoroutine(Blinks());
                StartCoroutine(PowerDown());
                
            };break;

            case PlayerPower.Basic:{
                die();
            }; break;
        }
    }

    /// <summary>
    /// Salto más pequeño posible de dar
    /// </summary>
    public void miniJump() {
        // velocidad = raíz_cuadrada(2 * gravedad * distancia a recorrer)
        playerRigidbody2D.velocity = new Vector2(playerRigidbody2D.velocity.x, Mathf.Sqrt(2 * Physics2D.gravity.magnitude * jumpHeight));
    }

    /// <summary>
    /// Provoca que el jugador muera
    /// </summary>
    public void die() {
        AudioAgent.damageTaken.GetComponent<AudioSource>().Play();
        GameObject.Find("InGameSong").GetComponent<AudioSource>().Stop();
        playerRigidbody2D.velocity = new Vector2(0f, Mathf.Sqrt(2 * Physics2D.gravity.magnitude * jumpHeight));
        disableColliders();
        GetComponent<PlayerMovement>().enabled = false;
        playerAnimator.SetBool("Anim_alive", false);
        Invoke("requestNextRound", 3);
    }

    /// <summary>
    /// Dibuja el collider que actua como pies del jugador para depurar mejor
    /// </summary>
    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(groundCollider.transform.localPosition + (Vector3)groundCollider.offset * groundCollider.transform.localScale.x, groundCollider.radius*groundCollider.transform.localScale.x);
    }

    /// <summary>
    /// Acciones realizadas al chocar
    /// </summary>
    /// <param name="col">Detalles de la colisión</param>
    void OnCollisionEnter2D(Collision2D col) {
        if (col.otherCollider == headCollider) { // Si choca con la cabeza, corta el salto
            stopJump = true;
        }

        if(col.transform.tag == "hellfire") {   // Si cae al vacío, muere
            die();
        }
    }

    /// <summary>
    /// Hace inmune al jugador mientras parpadea durante 1 segundo
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator Blinks() {
        immune = true;
        int layerMaskPlayer = Physics2D.GetLayerCollisionMask(9);
        int layerMaskEnemies = Physics2D.GetLayerCollisionMask(10);
        ignoreCollisionWithEnemies();
        playerAnimator.speed = 0f;
        for (int i = 0; i < 10; i++) {
            GetComponent<SpriteRenderer>().enabled = !GetComponent<SpriteRenderer>().enabled;
            yield return new WaitForSeconds(0.1f);
        }

        GetComponent<SpriteRenderer>().enabled = true;
        playerAnimator.speed = 1f;
        Physics2D.SetLayerCollisionMask(9, layerMaskPlayer);
        Physics2D.SetLayerCollisionMask(10, layerMaskEnemies);
        immune = false;
    }

    /// <summary>
    /// Hace más grande al jugador, le cambia el color y aumenta su poder
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator PowerUp() {

        currentPower = PlayerPower.BigBasic;
        GetComponent<SpriteRenderer>().color = new Color32(111, 111, 110, 255);
        for (int i = 0; i < 4; i++) {
            transform.localScale = new Vector3(transform.localScale.x + powerUpScale/4, transform.localScale.y + powerUpScale/4, transform.localScale.z);
            yield return new WaitForSeconds(0.3f);
        }
        
    }

    /// <summary>
    /// Hace más pequeño al jugador, le cambia el color y disminuye su poder
    /// </summary>
    /// <returns>IEnumerator</returns>
    IEnumerator PowerDown() {

        currentPower = PlayerPower.Basic;
        GetComponent<SpriteRenderer>().color = new Color32(255, 255, 255, 255);
        for (int i = 0; i < 4; i++) {
            transform.localScale = new Vector3(transform.localScale.x - powerUpScale / 4, transform.localScale.y - powerUpScale / 4, transform.localScale.z);
            yield return new WaitForSeconds(0.3f);
        }
        
    }

    /// <summary>
    /// Aumenta el poder siempre y cuando no lo tenga ya aumentado
    /// </summary>
    public void pickUpMushroom() {
        if (currentPower == PlayerPower.Basic) {
            StartCoroutine(PowerUp());
        }
    }

    /// <summary>
    /// Deshabilita todos los colliders del jugador
    /// </summary>
    void disableColliders() {
        foreach (Collider2D collider in GetComponents<Collider2D>()) {
            collider.enabled = false;
        }
    }

    /// <summary>
    /// Indica al GamePlayManager que esta ronda debe terminar
    /// </summary>
    void requestNextRound() {
        GameObject.FindGameObjectWithTag("gameplaymanagement").GetComponent<GameplayManager>().roundOver();
    }

    /// <summary>
    /// Ignora colisiones con los enemigos
    /// </summary>
    void ignoreCollisionWithEnemies() {
        Physics2D.IgnoreLayerCollision(9, 10);
    }

    /// <summary>
    /// Aumenta la velocidad
    /// </summary>
    /// <param name="doing">¿Pulsando shift?</param>
    public void Sprinting(bool doing) {
        if (doing) {
            speed = speed + extraSpeedOnSprint;
            
        } else {
            speed = speed - extraSpeedOnSprint;
        }
        playerAnimator.SetBool("Anim_sprint", doing);
    }
    

}
