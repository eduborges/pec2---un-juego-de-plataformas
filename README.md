Super Inferno Hotdog
====================

Este proyecto es la PEC2 de la asignatura *Programación Unity 2D* del *Master en diseño y desarrollo de videojuegos* de la *Universitat Oberta de Catalunya*.

Es una imitación del nivel 1-1 de [Super Mario Bros](https://www.youtube.com/watch?v=PsC0zIhWNww).

Structure & Implementation
--------------------------

**Animaciones**:

Se ha utilizado la técnica de rigging para animar el personaje principal.

Además para lograrlo se ha optado por utilizar el paquete del Package Manager de Unity: **2D Animation**.
El cuál permite generar un esqueleto para un Sprite en el Sprite Editor.

El resto de animaciones han sido pequeños desplazamientos o rotaciones del sprite (bloques, enemigos, monedas), algunas modificando el transform con coroutines y otras creando las animaciones en el Animator.

**Seguimiento de Cámara**:

La cámara sigue constantemente al personaje a partir de que este llegue al centro de la pantalla, pero nunca retrocediendo.
Para conseguir este efecto se ha utilizado el paquete del Package Manager de Unity: **Cinemachine**

Además para evitar el retroceso del personaje en el borde izquierdo de la cámara se ha añadido un collider que solo afecta a los Layers tipo Player, teniendo la capacidad de recolocarse dependiendo del tamaño de la pantalla o resolución del usuario gracias al script [CameraHelper.cs](Assets/Scripts/CameraHelper.cs)

**Construcción del mapa**:

Para construir el terreno básico (no interactuable) se han utilizado un conjunto de Tilemaps2D (uno para el suelo, otro para las paredes y otro para los detalles).

Para construir los objetos interactuables se han creado prefabricados (bloques, enemigos, powersUps...) y se han colocado en la escena.

**Movimiento de personaje**:

Se ha modificado el vector velocidad (velocity) del rigidbody del personaje dependiendo del valor del eje horizal (horizontalAxis)

Cuando el personaje está en el aire el vector velocidad se modifica mediante SmoothDamp que genera una interpolación entre la velocidad actual y la deseada (ya que en el aire el movimiento no debe ser tan inmediato)

**Salto de personaje**:

Se ha implementado que el personaje salte dependiendo del tiempo que se mantenga pulsada la tecla de salto.

Se ha modificado la componente Y del vector velocidad de manera que tendrá la velocidad necesaria para dar el salto más pequeño posible calculado con la siguiente fórmula: v = √(2*g*d).
Donde v = velocidad , g = gravedad y d = distancia (altura a recorrer). Por lo que solo es necesario especificar hasta que altura puede ser el salto más pequeño.

El salto más grande es un conjunto de saltos pequeños.

**Enemigos**:

Tienen una velocidad constante horizontal, se ha modificado su vector velocity.

Cuando triggerea un collider que tienen en la cabeza son aplastados.

Cuando chocan contra paredes cambian su sentido de movimiento y cuando chocan contra el layer Player, le hacen daño.

**Bloques**:

Existe en prefabricado bloque que dependiendo de lo que se seleccione en el inspector se puede convertir en un tipo de bloque u otro de manera dinámica.

Para conseguir esto tienen un atributo de tipo Block que es una clase base de la que heredan.

_Esquema de herencia_:

![inheritBlock](ReadmeImages/inherit.png)

Los bloques tienen 3 colliders para facilitar su comportamiento, uno superior, uno inferior y el principal.

**Destrucción dinámica de GameObject**:

Algunos objectos se destruyen con el tiempo, por ejemplo, los efectos de destrucción del bloque, pero otros como enemigos y powerUps se destruyen al tocar un collider invisible que hay debajo del mapa (al que llegan al caer al vacío).

**Gestión de UI, tiempos y pantallas(vidas,gameover,pause)**

Entre [GamePlayManager.cs](Assets/Scripts/GamePlayManager.cs), [PauseAgent.cs](Assets/Scripts/PauseAgent.cs) y [UIManager.cs](Assets/Scripts/UIManager.cs) llevan el grueso de este trabajo, utilizando como recurso nuevo las llamadas Invoke, CancelInvoke, InvokeRepeating.

How to play
------------

- Reach de flag at the end of the level before time ends
- Get the maximun number of points and coins before you reach the flag
- Dont fall and dont let enemys hit you

Controls
--------

**Jump** - ↑

**Horizontal movement** - ← →

**Sprint** - ⇪ _Shift_

Play [here](https://alu0100885613.github.io/SuperInfernoHotdog/)

Gameplay
--------

[![U2D](https://i.ytimg.com/vi/HoJuHTVuHkI/hqdefault.jpg)](https://www.youtube.com/watch?v=HoJuHTVuHkI "Super Inferno Hotdog")

_Click image to access Gameplay video_

Support
-------

mail: <eduborges@uoc.edu>

Author
------

[Eduardo Borges Fernández](https://gitlab.com/eduborges)

Credits
-------

**Music**:

- [Byzantine (loop)](https://opengameart.org/content/byzantine-loop) by [t4ngr4m](https://opengameart.org/users/t4ngr4m) is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)

License
-------

The project is licensed under [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/).